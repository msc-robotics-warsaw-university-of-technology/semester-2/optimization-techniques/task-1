# AMPL MODEL file for Bicycle company plan

option solver cplex;

# Declarations of sets and parameters
set MONTH;
set CAPACITY;

param max_capacity {CAPACITY};
param init_stored;
param prod_cost {CAPACITY};
param store_cost;
param sales {MONTH};


# Decision variable array
var To_be_produced {m in MONTH, c in CAPACITY} >= 0, <= max_capacity[c];

# Problem variables
var Cost_prod {m in MONTH} = sum {c in CAPACITY} prod_cost[c]*To_be_produced[m, c];
var p {m in MONTH} = sum {c in CAPACITY} To_be_produced[m, c];
var Sales_balance {m in MONTH} = p[m] - (sales[m]*1000);
var To_be_Stored {m in MONTH} = (if m = 1 then init_stored else To_be_Stored[m-1]) + Sales_balance[m];
var Cost_store {m in MONTH} = store_cost*To_be_Stored[m];

# Objective
minimize Cost: sum {m in MONTH} (Cost_prod[m] + Cost_store[m]);

# Constraints on always satisfying the monthly demand
subject to SatisfyDemand {m in MONTH}: To_be_Stored[m] >= 0